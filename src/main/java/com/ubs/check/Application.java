package com.ubs.check;

import com.ubs.check.service.ItemsService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {

    @Bean
    ItemsService getItemsService() {
        return new ItemsService();
    }

    public static void main(String... args) {
        SpringApplication.run(Application.class);
    }
}
