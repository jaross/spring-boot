package com.ubs.check.service;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Multimaps;
import com.ubs.check.model.Item;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import static java.util.Arrays.asList;

public class ItemsService {


    Map<Long, Item> getTables(Collection<Item> collection) {
        return Maps.uniqueIndex(collection, new Function<Item, Long>() {
            @Override
            public Long apply(Item item) {
                return item.getId();
            }
        });
    }

    Map<Long, Collection<Item>> getGroupedTables(Collection<Item> collection) {

        ImmutableMap<Long, Collection<Item>> m = Multimaps.index(collection, new Function<Item, Long>() {
            @Override
            public Long apply(Item item) {
                return item.getId() == null ? 0L : item.getId();
            }
        }).asMap();


        TreeMap<Long, Collection<Item>> res = new TreeMap<Long, Collection<Item>>(new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                if (o1.equals(o2)) {
                    return 0;
                }
                if (o1.equals(0L)) {
                    return 1;
                }

                if (o2.equals(0L)) {
                    return -1;
                }

                return o1.compareTo(o2);
            }
        });
        res.putAll(m);

        return res;
    }

    public Map<Long, Collection<Item>> getGroupedTables() {
        Item one = new Item(1L, "one");
        Item two = new Item(null, "two");
        Item three = new Item(1L, "three");
        Item four = new Item(2L, "four");

        return getGroupedTables(asList(one, two, three, four));
    }
}
