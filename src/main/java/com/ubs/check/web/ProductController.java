package com.ubs.check.web;


import com.ubs.check.model.Greeting;
import com.ubs.check.service.ItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping("/")
public class ProductController {


    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private ItemsService service;

    @RequestMapping(value = "home", method = RequestMethod.GET)
    @ResponseBody
    public Greeting home() {
        return new Greeting(counter.incrementAndGet(), "hello, world");
    }

    @RequestMapping(value = "re", method = RequestMethod.GET)
    public ModelAndView in() {
        return new ModelAndView("test.jsp");
    }


    @RequestMapping("array")
    @ResponseBody
    public String array(@RequestParam("options") String statuses) {
        System.out.println(statuses);
        return "ok";
    }

    @RequestMapping("key/{name}")
    @ResponseBody
    public Greeting greetByName(@PathVariable("name") String name) {
        return new Greeting(counter.incrementAndGet(), "hello, " + name);
    }

    @RequestMapping("map")
    @ResponseBody
    public Map getMap() {
        return service.getGroupedTables();
    }

}
