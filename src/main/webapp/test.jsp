<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>hello</title>
    <script type="text/javascript" src="jquery-2.1.1.js"></script>
    <script type="text/javascript" src="jquery-ui.js"></script>
    <link href="jquery-ui.css" rel="stylesheet">
    <style>
        .ui-button.ui-state-active.added {
            background: #8F8;
        }
        .ui-button.ui-state-active.deleted {
            background: #F99;
        }
        .ui-button.ui-state-active.changed {
            background: yellow;
        }
        .ui-button.ui-state-active.unchanged {
            background: #dddddd;
        }
        .ui-button {
            color: #000;
            border: none;
        }
    </style>
</head>
<body>

<div style="margin-left: 100px;">
    <label for="name">The message: </label><input type="text" id="name"/>
</div>

<div class="checks">
    <input type="checkbox" checked id="A"/><label class="added" for="A">Added</label>
    <input type="checkbox" checked id="D"/><label class="deleted" for="D">deleted</label>
    <input type="checkbox" checked id="C"/><label class="changed" for="C">changed</label>
    <input type="checkbox" checked id="U"/><label class="unchanged" for="U">unchanged</label>
</div>

<button id="sender">Go</button>

<script type="text/javascript">
    var message = document.location.hash.replace('#', '');
    $('#name').val(message);
    document.location.hash = '';

    $('.checks').buttonset();

    $('#sender').click(function () {
        var types = [];

        $('.checks').find('input:checkbox').each(function () {
            if ($(this).is(':checked')) {
                types.push(this.id);
            }
        });

        console.log(types);

        $.ajax({
            url: '/array',
            data: {options : types.join()}
        })
    });
</script>

</body>
</html>