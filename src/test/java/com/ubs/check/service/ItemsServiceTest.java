package com.ubs.check.service;

import com.ubs.check.model.Item;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class ItemsServiceTest {


    private final ItemsService service = new ItemsService();

    @Test
    public void testGetTables() {

        Item one = new Item(1L, "one");
        Item two = new Item(2L, "two");
        Item three = new Item(3L, "three");
        Item four = new Item(4L, "four");
        Collection<Item> list = asList(one, two, three, four);

        Map<Long, Item> table = service.getTables(list);

        assertEquals(4, table.size());
        assertEquals(one, table.get(1L));
        assertEquals(two, table.get(2L));
        assertEquals(three, table.get(3L));
        assertEquals(four, table.get(4L));
    }

    @Test
    public void testGetGroupedTables() {
        Item one = new Item(1L, "one");
        Item two = new Item(null, "two");
        Item three = new Item(1L, "three");
        Item four = new Item(2L, "four");
        Item five = new Item(5L, "five");

        Collection<Item> list = asList(one, two, three, four, five);

        Map<Long, Collection<Item>> groupedTable = service.getGroupedTables(list);

        assertEquals(4, groupedTable.size());
        assertEquals(asList(two), groupedTable.get(0L));
        assertEquals(asList(one, three), groupedTable.get(1L));
        assertEquals(asList(four), groupedTable.get(2L));

        String name = "new name";
        groupedTable.get(2L).iterator().next().setName(name);

        assertEquals(name, groupedTable.get(2L).iterator().next().getName());

        assertThat(groupedTable.keySet(), IsIterableContainingInOrder.contains(1L, 2l, 5l, 0l));
    }
}
